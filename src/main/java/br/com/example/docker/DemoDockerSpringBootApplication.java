package br.com.example.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDockerSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDockerSpringBootApplication.class, args);
	}
}
